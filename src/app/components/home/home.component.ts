import { Component } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent {
  songs: any[] = [];
  loading: boolean;
  errorMessage: String = '';

  constructor(private spotify: SpotifyService) {
    this.loading = true;
    spotify.getNewRealeses().subscribe((data: any) => {
      this.songs = data;
      this.loading = false;
    }, (e) => {
      this.errorMessage = e.error.error.message;
      this.loading = false;
    });
  }
}
