import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styles: []
})
export class ArtistComponent {
  artist: any = {};
  tracks: any = [];
  loading: Boolean = false;
  constructor(private router: ActivatedRoute, private spotify: SpotifyService) {
        this.router.params.subscribe((params) => {
          // tslint:disable-next-line:prefer-const
          let id = params['id'];
          this.getArtist(id);
          this.getTopSongs(id);
        });
  }

  getArtist(id) {
    this.loading = true;
    this.spotify.getArtistById(id).subscribe((data: any) => {
      this.artist = data;
      this.loading = false;
    });
  }

  getTopSongs(id) {
    this.spotify.getTopSongsArtist(id).subscribe((data: any) => {
      console.log(data);
      this.tracks = data;
    });
  }

}
