import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class SpotifyService {
  constructor(private http: HttpClient) {}

  getQuery(query: string) {
    // tslint:disable-next-line:no-inferrable-types
    const url: string = `https://api.spotify.com/v1/${query}`;

    const headers: HttpHeaders = new HttpHeaders({
      // tslint:disable-next-line:max-line-length
      'Authorization': 'Bearer BQB7qEb8AY1Cc8cOf3HfaNTtS7AYS9n9w6wgPKiruTV4feQ5aHMASgA0DChCgWU2HWLFUyR1nEBkKqCU4CdP3Kjei1lH2cMQYbWxoTJvvWzX4MknuE7uHU5IfgHhjZeUFKx-lkX3g2YanYE'
    });

    return this.http.get(url, {headers: headers});

  }

  getNewRealeses() {
    return this.getQuery('browse/new-releases')
                .pipe(map(data => data['albums'].items));
  }

  getArtistByTerm(term) {
    return this.getQuery(`search?q=${term}&type=artist&limit=15&offset=5`)
                .pipe(map(data => data['artists'].items));
  }

  getArtistById(id) {
    return this.getQuery(`artists/${id}`)
                .pipe(map(data => data));
  }

  getTopSongsArtist(id) {
    return this.getQuery(`artists/${id}/top-tracks?country=ES`)
                .pipe(map(data => data['tracks']));
  }
}
